#!/bin/sh

NUM1=$1
NUM2=$2
NUM3=$3

echo "($NUM1 ^ $NUM2) + $NUM3 * $NUM2"
NEWNUM=$NUM1
i=0
while [ $i -lt $NUM2 ]
do
NEWNUM=`expr  $(( NEWNUM * $NUM1 ))`
i=`expr $i + 1`
done
Result=`expr $NEWNUM + $(( $NUM3 * $NUM2 ))`
echo "Result is : $Result"
if [ ! -d "build/" ]
then
mkdir build/
fi
if [ -z ${OUTPUT_FILE_NAME} ]
then
OUTPUT_FILE_NAME="output.txt"
fi
echo  $Result > build/$OUTPUT_FILE_NAME
